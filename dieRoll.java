public class dieRoll{
	public static void main (String [] args)
	{
	 int die1;
	 int die2;
	
	boolean rollContinue = true;
	int rollCount = 0;
	
	while (rollContinue)
	{
		die1 = (int)(Math.random()*6) + 1;
		die2 = (int)(Math.random()*6) + 1;
		System.out.println( "Result of rolling the dice: " + die1 +" and " + die2);
		rollCount++;
		
		if (die1 == 1 && die2 == 1)
		{
			System.out.println( "Snake eyes were reached after " + rollCount + " rolls of the dice");
			rollContinue = false;
			} 
		else
			System.out.println( "No snake eyes as of yet");
	}
	
}	
}